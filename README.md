# Corry_analysis

## Getting started
First source the appropriate software on an ALmalinux9 machine:
```
source  /cvmfs/sft.cern.ch/lcg/releases/ROOT/6.28.04-740f6/x86_64-el9-clang16-opt/setup.sh
```

Executing the program from the base directory with:
```
corry -c config/start_reconstruction.conf
```



## Reconstruction using EUDAQ2 raw data
```
git clone https://github.com/tbisanz/eudaq.git
git checkout bdaq_stcontrol
```

eudaq installation (bdaq_stcontrol branch)
```
cmake -DUSER_BUILD_TLU_ONLY_CONVERTER=ON  -DUSER_BDAQ53_BUILD=ON ..
```

corryvreckan installation
create build and remove the version (2.6.1) in the cmake file:

then cmake and amke install
```
cmake -Deudaq_DIR=/eos/user/c/chkrause/in_time_setup/april_testbeam/eudaq/cmake -DBUILD_EventLoaderEUDAQ2=ON -DBUILD_OnlineMonitor=OFF ..
```
